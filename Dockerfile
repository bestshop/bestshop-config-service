FROM java:8-jre-alpine

EXPOSE 8888

ADD ./target/bestshop-config-service.jar /app/

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app/bestshop-config-service.jar"]